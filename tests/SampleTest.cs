using System;
using System.Collections.Generic;
using Xunit;
using FluentAssertions;
using HumbleProgrammer;
using HumbleProgrammer.Characters;
using Xunit.Abstractions;

namespace Tests
{
    public class SampleTest
    {
        private readonly ITestOutputHelper _testOutputHelper;

        public SampleTest(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
        }
        
        [Theory]
        [InlineData(5), InlineData(0), InlineData(10)]
        public void FlashImplementations(int wins)
        {
            var barryAllen = new Flash(wins,"Flash");
            
            barryAllen.Cooldown.Should().BePositive();
            barryAllen.Damage.Should().BePositive();
            barryAllen.Hp.Should().BePositive();
            barryAllen.Speed.Should().BePositive();
        }

        [Theory]
        [InlineData(5, 75), InlineData(0, 50), InlineData(10, 100)]
        public void FlashDamage(int wins, int dmg)
        {
            var barryAllen = new Flash(wins,"Flash");
            
            barryAllen.Damage.Should().Be(dmg);
        }
        
        [Theory]
        [InlineData(5, 51), InlineData(0, 1), InlineData(10, 101)]
        public void FlashSpeed(int wins, int speed)
        {
            var barryAllen = new Flash(wins,"Flash");
            
            barryAllen.Speed.Should().Be(speed);
        }

        [Fact]
        public void PlayerUnits()
        {
            var id = Guid.NewGuid();
            var mocker = new MockCharacter("One",100, 50);
            var characters = new List<Character> {mocker};
            var player1 = new Player(id, characters,"One");

            player1.Characters[0].Should().Be(characters[0]);
        }
        
        [Fact]
        public void GameWinner()
        {
            var id = Guid.NewGuid();
            var mocker = new MockCharacter("One",100, 50);
            var characters1 = new List<Character> {mocker};
            var characters2 = new List<Character>();
            var player1 = new Player(id, characters1,"One");
            var player2 = new Player(id, characters2,"Two");
            var newGame = new Game(player1, player2);
            
            newGame.CheckWinner(player1.Characters, player2.Characters).Should().Be(Game.Winner.Player1);
            newGame.CheckWinner(player2.Characters, player1.Characters).Should().Be(Game.Winner.Player2);
            newGame.CheckWinner(player1.Characters, player1.Characters).Should().Be(Game.Winner.None);
        }
        
        [Theory]
        [InlineData(0, 100), InlineData(1, 0)]
        public void GameAttack(int step, int hp)
        {
            var id = Guid.NewGuid();
            var mock1 = new MockCharacter("One",100, 50);
            var mock2 = new MockCharacter("Two",100, 50);
            var mock3 = new MockCharacter("Three",100, 50) {Hp = 50};
            var characters1 = new List<Character> {mock1};
            var characters2 = new List<Character> {mock2, mock3};
            var player1 = new Player(id, characters1,"One");
            var player2 = new Player(id, characters2,"One");
            var newGame = new Game(player1, player2);

            var logs = newGame.Attack(step, player1.Characters[0], player2.Characters[0],player1.Characters, player2.Characters);

            foreach (var log in logs)
            {
                _testOutputHelper.WriteLine(log);
            }
            
            player2.Characters[0].Hp.Should().Be(hp);
        }
        
        [Theory]
        [InlineData(0, 120), InlineData(1, 50)]
        public void GameStep(int step, int hp)
        {
            var id = Guid.NewGuid();
            var mock1 = new MockCharacter("One",100, 50);
            var mock2 = new MockCharacter("Two",100, 50);
            var mock3 = new MockCharacter("Three",100, 50) {Hp = 50};
            var characters1 = new List<Character> {mock1};
            var characters2 = new List<Character> {mock2, mock3};
            var player1 = new Player(id, characters1,"One");
            var player2 = new Player(id, characters2,"Two");
            var newGame = new Game(player1, player2);

            var p1 = player1.Characters;
            var p2 = player2.Characters;
            var logs = newGame.Step(step, ref p1, ref p2);
            player1.Characters = p1;
            player2.Characters = p2;

            foreach (var log in logs)
            {
                _testOutputHelper.WriteLine(log);
            }
            
            player2.Characters[0].Hp.Should().Be(hp);
        }

        [Fact]
        public void GameDeathCheck()
        {
            var id = Guid.NewGuid();
            var mock1 = new MockCharacter("One",100, 50);
            var mock2 = new MockCharacter("Two",100, 50);
            var mock3 = new MockCharacter("Three",100, 50) {Hp = 0};
            var characters1 = new List<Character> {mock1};
            var characters2 = new List<Character> {mock2, mock3};
            var player1 = new Player(id, characters1, "One");
            var player2 = new Player(id, characters2,"Two");
            var newGame = new Game(player1, player2);
            var i = 0;
            newGame.DeathCheck(ref characters2, ref i);

            characters2.Count.Should().Be(1);
        }
        
        [Fact]
        public void GameSimulate()
        {
            var id = Guid.NewGuid();
            var mock1 = new MockCharacter("One",100, 50);
            var mock2 = new MockCharacter("Two",100, 50);
            var mock3 = new MockCharacter("Three",100, 50) {Hp = 50};
            var characters1 = new List<Character> {mock1};
            var characters2 = new List<Character> {mock2, mock3};
            var player1 = new Player(id, characters1,"One");
            var player2 = new Player(id, characters2,"Two");
            var newGame = new Game(player1, player2);

            var (_,b) = newGame.SimulateMatch();
            b.Should().Be(player2);
        }
    }

    class MockCharacter : Character
    {
        public string Name { get; }
        public int Cooldown { get; } = 100;
        public int Damage { get; }
        public int Hp { get; set; } = 100;
        public int Speed { get; }
        public List<string> SpecialAction(List<Character> allies, List<Character> enemies)
        {
            var log = new List<string>();
            
            foreach (var ally in allies)
            {
                ally.Hp += 10;
                log.Add(this.Name + " healed " + ally.Name + " to " + ally.Hp);
            }
            
            return log;
        }

        public MockCharacter(string name, int dmg, int spd)
        {
            this.Speed = spd;
            this.Name = name;
            this.Damage = dmg;
        }
    }
}