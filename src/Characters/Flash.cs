using System;
using System.Collections.Generic;

namespace HumbleProgrammer.Characters
{
    public class Flash : Character
    {
        private readonly int _wins;
        public string Name { get; }

        public int Cooldown { get; } = 5;

        public int Damage => 50 + _wins * 5;

        public int Hp { get; set; } = 100;

        public int Speed => 1 + _wins * 10;

        public List<string> SpecialAction(List<Character> allies, List<Character> enemies)
        {
            var log = new List<string>();
            
            foreach (var ally in allies)
            {
                ally.Hp += 10;
                log.Add(this.Name + " healed " + ally.Name + " to " + ally.Hp);
            }
            
            return log;
        }

        public Flash(int wins, string name)
        {
            _wins = wins;
            Name = name;
        }
    }
}