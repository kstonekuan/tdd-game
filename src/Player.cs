using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using HumbleProgrammer.Characters;

namespace HumbleProgrammer
{
    [ExcludeFromCodeCoverage]
    public class Player
    {
        
        public Guid Id { get; }
        public List<Character> Characters { get; set; }
        public string Name { get; }

        public Player(Guid id, List<Character> characters, string name)
        {
            Id = id;
            Characters = characters;
            Name = name;
        }
        
    }
}