using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using HumbleProgrammer.Characters;

namespace HumbleProgrammer
{
    [ExcludeFromCodeCoverage]
    public class Game
    {
        private readonly Player _player1;
        private readonly Player _player2;

        public Game(Player player1, Player player2)
        {
            _player1 = player1;
            _player2 = player2;
        }

        public enum Winner
        {
            Player1,
            Player2,
            None,
        };

        // Check if there is a winner, or not
        public Winner CheckWinner(List<Character> player1Units, List<Character> player2Units)
        {
            if (player1Units.Count != 0 && player2Units.Count == 0) return Winner.Player1;
            if (player1Units.Count == 0 && player2Units.Count != 0) return Winner.Player2;
            return Winner.None;
        }

        // Should run through a whole battle, return the logs of the battle and return the winning player!
        public (List<string>, Player) SimulateMatch()
        {
            var step = 0;
            var starter = 0;
            var speedOne = 0;
            var speedTwo = 0;
            var log = new List<string>();
                
            foreach (var character in _player1.Characters) speedOne += character.Speed;
            foreach (var character in _player2.Characters) speedTwo += character.Speed;

            if (speedOne < speedTwo) starter = 1;

            while (CheckWinner(_player1.Characters, _player2.Characters) == Winner.None)
            {
                log.Add("Round " + step);
                if (step + starter % 2 == 0)
                {
                    var p1 = _player1.Characters;
                    var p2 = _player2.Characters;
                    log.AddRange(Step(step, ref p1,ref p2));
                    _player1.Characters = p1;
                    _player2.Characters = p2;
                }
                else
                {
                    var p1 = _player1.Characters;
                    var p2 = _player2.Characters;
                    log.AddRange(Step(step, ref p2,ref p1));
                    _player1.Characters = p1;
                    _player2.Characters = p2;
                }

                step++;
            }

            if (CheckWinner(_player1.Characters, _player2.Characters) == Winner.Player1)
                return (log, _player1);
            return (log, _player2);
        }

        // Based on the number of steps ran, it should decide to cast skill or do normal attack, and return the logs
        // for number of steps taken
        public List<string> Step(int step, ref List<Character> player1Units, ref List<Character> player2Units)
        {
            int i = 0, j = 0;
            bool changed1 = true, changed2 = true;
            var log = new List<string>();

            while ((changed1 || changed2) && player1Units.Count != 0 && player2Units.Count != 0)
            {
                if (i < player1Units.Count)
                {
                    log.AddRange(Attack(step, player1Units[i++], player2Units[0], player1Units, player2Units));
                    
                    var deathLog1 = DeathCheck(ref player2Units, ref j);

                    log.AddRange(deathLog1);
                    
                }
                else changed1 = false;

                if (j < player2Units.Count)
                {
                    log.AddRange(Attack(step, player2Units[j++], player1Units[0], player2Units, player1Units));

                    var deathLog2 = DeathCheck(ref player1Units, ref i);

                    log.AddRange(deathLog2);
                }
                
                else changed2 = false;
            }
            
            return log;
        }

        public List<string> Attack(int step, Character character1, Character character2, List<Character> player1Units, List<Character> player2Units)
        {
            var log = new List<string>();
            if (step % character1.Cooldown == 0) log.AddRange(character1.SpecialAction(player1Units, player2Units));
            else
            {
                character2.Hp -= character1.Damage;
                log.Add(character1.Name + " damaged " + character2.Name + " to " + character2.Hp);
            }

            return log;
        }

        public List<string> DeathCheck(ref List<Character> playerUnits, ref int i)
        {
            var logs = playerUnits
                .Where(x => x.Hp < 1)
                .Select(x => x.Name + " got rekt")
                .ToList();

            var nextAlive = playerUnits
                .Skip(i)
                .FirstOrDefault(x => x.Hp > 0);

            playerUnits = playerUnits
                .Where(x => x.Hp > 0)
                .ToList();

            for (i = 0; i < playerUnits.Count; i++)
            {
                if (playerUnits[i] == nextAlive) break;
            }
            
            return logs;
        }
    }
}