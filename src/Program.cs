﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using HumbleProgrammer.Characters;

namespace HumbleProgrammer
{
    [ExcludeFromCodeCoverage]
    class Program
    {
        public static void Main(string[] args)
        {
            var id = Guid.NewGuid();
            var barry = new Flash(0,"Barry");
            var jay = new Flash(0,"Jay");
            var iris = new Flash(0,"Iris");
            var oliver = new Arrow(0,"Oliver");
            var john = new Arrow(0, "John");
            var characters1 = new List<Character> {barry, oliver, john};
            var characters2 = new List<Character> {iris, jay};
            var player1 = new Player(id, characters1, "One");
            var player2 = new Player(id, characters2, "Two");
            var newGame = new Game(player1, player2);

            var (log,winner) = newGame.SimulateMatch();

            Console.WriteLine(log.Aggregate((x,y) => x + "\n" + y));
            Console.WriteLine(winner.Name + " Won");
        }
    }
}